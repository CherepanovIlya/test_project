from django.shortcuts import render
from .models import ResponseUrl, RequestUrl


def index(request):
    responses = ResponseUrl.objects.all().prefetch_related()

    return render(request, 'url_parser/index.html', {'responses': responses})
