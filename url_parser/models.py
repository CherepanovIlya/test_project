from datetime import datetime
from django.db import models
from .server import ParsingServer


# Записи о запросах
# Во время сохранения передает запрос на сервер запросов, который сохраняет запрос в очередь
class RequestUrl(models.Model):
    url = models.URLField(blank=False, null=False)
    time_to_request = models.DateTimeField(blank=True, default=datetime.now)

    def save(self, *args, **kwargs):
        if self.time_to_request is None:
            self.time_to_request = datetime.now()


        super(RequestUrl, self).save(*args, **kwargs)
        ParsingServer.get_instance().add_to_server(self)

    def __str__(self):
        return self.url


# Записи о выполенных запросах
class ResponseUrl(models.Model):
    STATUS_ERROR = 'er'
    STATUS_OK = 'ok'

    RESPONSE_STATUS_CHOICES = [
        (STATUS_OK, 'OK'),
        (STATUS_ERROR, 'ERROR'),
    ]

    request_url = models.OneToOneField(RequestUrl, on_delete=models.CASCADE)
    response_status = models.CharField(max_length=2, choices=RESPONSE_STATUS_CHOICES, default=STATUS_ERROR)
    title = models.CharField(max_length=255, blank=True, null=True)
    encoding = models.CharField(max_length=15, blank=True, null=True)
    h1 = models.CharField(max_length=255, blank=True, null=True)

    def response_content(self):
        content = [
            self.title,
            self.encoding,
            self.h1
        ]

        return ', '.join(filter(None, content))

    def has_content(self):
        return bool(self.response_content())

    def __str__(self):
        return self.request_url.url


# Записи очереди для сервера запросов
# Используется сервером запросов для сохранения данных очереди
# для возобновления работы после остановки приложения
class ParsingServerQueue(models.Model):
    request_url = models.OneToOneField(RequestUrl, on_delete=models.CASCADE)
