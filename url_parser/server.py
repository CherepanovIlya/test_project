import time
from datetime import datetime
from queue import Queue
from threading import Thread
import requests
from bs4 import BeautifulSoup as bs


# Обертка для автоматического запуска сервера запрососов
def get_parser_wsgi_application(wsgi_application):
    ParsingServer.start_server()
    return wsgi_application()


# Сервер запросов работает в отдельном потоке, получая запросы через объект Queue
# Порядок работы:
# 1. Заполнение очереди запросами из базы данных и запуск петли обработки запросов в отдельном потоке
# 2. Петля выполняет проверку Queue на наличие запросов в течение каждой секунды и проверяет их время исполнения.
# 3. В необходимое время петля выполняет запрос и вносит полученные данные в БД, после удаляет запрос из очереди.
class ParsingServer:
    @classmethod
    def start_server(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = ParsingServer()

    @classmethod
    def get_instance(cls):
        cls.start_server()
        return cls.instance

    def __init__(self):
        self.queue = Queue()
        self.serverThread = Thread(target=self.run_server_loop)

        self.initialize_queue()
        self.serverThread.start()

    def initialize_queue(self):
        from .models import ParsingServerQueue
        parse_server_queue = ParsingServerQueue.objects.all().prefetch_related()

        for parse_server_queue_item in parse_server_queue:
            self.queue.put(parse_server_queue_item.request_url)

    def add_to_server(self, request):
        from .models import ParsingServerQueue

        ParsingServerQueue.objects.create(request_url=request)
        self.queue.put(request)

    def run_server_loop(self):
        from .models import ParsingServerQueue

        requests_list = self.extract_from_queue()

        while True:
            time.sleep(1)

            requests_list = requests_list + self.extract_from_queue()
            for item in requests_list:
                if item.time_to_request.timestamp() <= datetime.now().timestamp():
                    if ParsingServerQueue.objects.filter(request_url=item).exists():
                        self.process_request(item)
                        ParsingServerQueue.objects.filter(request_url=item).delete()

                    requests_list.remove(item)

    def extract_from_queue(self):
        queue_list = []

        while True:
            if self.queue.empty():
                break

            queue_list.append(self.queue.get())

        return queue_list

    def process_request(self, request_url):
        from .models import ResponseUrl

        response = self.make_request(request_url)

        if response['status'] == 'error':
            ResponseUrl.objects.create(request_url=request_url, response_status=ResponseUrl.STATUS_ERROR)
            return

        ResponseUrl.objects.create(
            request_url=request_url,
            response_status=ResponseUrl.STATUS_OK,
            encoding=response['encoding'],
            h1=response['h1'],
            title=response['title']
        )

    def make_request(self, request_url):
        content = {
            'status': 'error'
        }

        try:
            request = requests.get(request_url.url)
        except Exception as e:
            return content

        if request.status_code == 200:
            soup = bs(request.text, 'html.parser')

            content['status'] = 'ok'
            content['encoding'] = request.encoding
            content['title'] = soup.find('title').get_text() if soup.find('title') else ''
            content['h1'] = soup.find('h1').get_text() if soup.find('h1') else ''

        return content
