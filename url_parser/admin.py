from django.contrib import admin
from .models import RequestUrl, ResponseUrl

admin.site.register(ResponseUrl)


class RequestUrlAdmin(admin.ModelAdmin):
    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['url', 'time_to_request']

        return []

    def has_change_permission(self, request, obj=None):
        return False


admin.site.register(RequestUrl, RequestUrlAdmin)
