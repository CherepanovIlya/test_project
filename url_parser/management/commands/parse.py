from django.core.management.base import BaseCommand
from url_parser.server import ParsingServer


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('json', nargs=1)

    def handle(self, *args, **options):
        if options['json']:
            pass
