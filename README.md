# Тестовый проект

Для запуска приложения необходим Python >=3.5.
Запуск производится коммандой:

`
python manage.py runserver
`

Код сервера для запросов находится в модуле url_parser.server.
Главная страница по адресу `/index`

Ответ для задания 3.
```sql
INSERT INTO table_4 (InternalNumber, `Name/Surname`, Position, `Salary/Month`, Tax, Month)
SELECT InternalNumber, CONCAT(table_1.Name, ' ', table_1.Surname) as `Name/Surname`, Position, (`Salary/year` / 12) AS `Salary/Month`, Taxes as Tax, Month
FROM table_1
INNER JOIN table_2 ON table_1.id = table_2.employeeid
INNER JOIN table_3 ON table_1.id = table_3.employeeid;
```